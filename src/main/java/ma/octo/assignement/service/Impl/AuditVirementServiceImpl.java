package ma.octo.assignement.service.Impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.AuditVirement;
import ma.octo.assignement.domain.util.TypeEvenement;
import ma.octo.assignement.repository.AuditVirementRepository;
import ma.octo.assignement.service.AuditVirementService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Slf4j
@AllArgsConstructor
@Service
public class AuditVirementServiceImpl implements AuditVirementService {
    private final AuditVirementRepository auditVirementRepository;

    @Override
    public AuditVirement save(AuditVirement auditVirement) {
        log.info("Audit de l'événement {}", TypeEvenement.VIREMENT);
        log.info("Sauvegarde Audit virement  {}", auditVirement);
        return auditVirementRepository.save(auditVirement);
    }

    @Override
    public List<AuditVirement> findAll() {
        return auditVirementRepository.findAll();
    }
}
