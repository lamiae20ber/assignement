package ma.octo.assignement.web.Impl;

import lombok.AllArgsConstructor;
import ma.octo.assignement.domain.AuditVirement;
import ma.octo.assignement.service.AuditVirementService;
import ma.octo.assignement.utils.facades.FindAll;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/auditVirements")
public class AuditVirementController implements FindAll<List<AuditVirement>> {
    private final AuditVirementService auditVirementService;

    @GetMapping("/")
    @Override
    public List<AuditVirement> findAll() {
        return auditVirementService.findAll();
    }
}
