package ma.octo.assignement.mapper;

import ma.octo.assignement.exceptions.CompteNonExistantException;

import java.util.ArrayList;
import java.util.List;

public interface Mapper<DM, DTO> {
    DM convertToDM(DTO dto) throws CompteNonExistantException;

    DTO convertToDTO(DM dm);

    default List<DM> convertToDMs(List<DTO> dtos) throws CompteNonExistantException {
        List<DM> dms = new ArrayList<>();
        for (DTO dto : dtos) dms.add(convertToDM(dto));
        return dms;
    }

    default List<DTO> convertToDTOs(List<DM> dms) {
        List<DTO> dtos = new ArrayList<>();
        for (DM dm : dms) dtos.add(convertToDTO(dm));
        return dtos;
    }
}
