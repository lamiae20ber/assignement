package ma.octo.assignement.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ma.octo.assignement.domain.util.TypeEvenement;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@NoArgsConstructor
@Table(name = "AUDIT_VERSEMENT")
public class AuditVersement extends Audit {
    public AuditVersement(String message) {
        super(message, TypeEvenement.VERSEMENT);
    }
}
