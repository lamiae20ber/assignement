package ma.octo.assignement.web.Impl;

import lombok.AllArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.utils.facades.FindAll;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/comptes")
public class CompteController implements FindAll<List<Compte>> {
    private final CompteService compteService;
    @GetMapping("/")
    @Override
    public List<Compte> findAll() {
        return compteService.findAll();
    }
}
