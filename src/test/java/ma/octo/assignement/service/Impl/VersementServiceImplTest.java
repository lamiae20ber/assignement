package ma.octo.assignement.service.Impl;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.AuditVersementService;
import ma.octo.assignement.service.CompteService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class VersementServiceImplTest {

    @Mock
    private VersementRepository versementRepository;
    @Mock
    private CompteService compteService;
    @Mock
    private AuditVersementService auditVersementService;
    @InjectMocks
    private VersementServiceImpl versementService;

    @Test()
    void saveOnSuccess() throws Exception {
        //GIVEN
        Utilisateur utilisateur = new Utilisateur(1L, "user1", "Male", "last1", "first1", null);
        Compte compte = new Compte(1L, "010000A000001000", "RIB1", new BigDecimal(5), utilisateur);
        Versement versement = new Versement(3L, new BigDecimal(200), new Date(), "BOUJAAFAR Lamiae", compte, "Assignment 2021");

        //WHEN
        when(versementRepository.save(any(Versement.class))).then(returnsFirstArg());
        Versement savedVersement = versementService.save(versement);

        //THEN
        assertThat(savedVersement.getMontant()).isSameAs(versement.getMontant());
    }

    @Test()
    void canGetAllVersement() {
        Utilisateur utilisateur = new Utilisateur(1L, "user1", "Male", "last1", "first1", null);
        Compte compte = new Compte(1L, "010000A000001000", "RIB1", new BigDecimal(5), utilisateur);

        Versement versement1 = new Versement(3L, new BigDecimal(200), new Date(), "BOUJAAFAR Lamiae", compte, "Assignment 2021");
        Versement versement2 = new Versement(3L, new BigDecimal(200), new Date(), "BOUJAAFAR sanna", compte, "Assignment 2021");


        List<Versement> versementList = Arrays.asList(versement1, versement2);

        when(versementRepository.findAll()).thenReturn(versementList);

        List<Versement> versements = versementService.findAll();

        assertEquals("Assignment 2021", versements.get(0).getMotif());
        assertEquals(new BigDecimal(200), versements.get(0).getMontant());

        assertEquals("Assignment 2021", versements.get(1).getMotif());
        assertEquals(new BigDecimal(200), versements.get(1).getMontant());


//        versementRepository.findAll();
//        verify(versementRepository).findAll();
    }
}