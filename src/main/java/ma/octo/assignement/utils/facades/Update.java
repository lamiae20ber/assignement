package ma.octo.assignement.utils.facades;

public interface Update<E> {
    E update(E e);
}
