package ma.octo.assignement.service;

import ma.octo.assignement.domain.AuditVirement;
import ma.octo.assignement.utils.facades.FindAll;
import ma.octo.assignement.utils.facades.Save;

import java.util.List;

public interface AuditVirementService extends Save<AuditVirement, AuditVirement>, FindAll<List<AuditVirement>> {
}
