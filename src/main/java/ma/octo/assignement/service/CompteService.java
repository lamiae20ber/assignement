package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.utils.facades.FindAll;
import ma.octo.assignement.utils.facades.FindById;
import ma.octo.assignement.utils.facades.Save;

import java.util.List;

public interface CompteService extends FindAll<List<Compte>>, FindById<Compte, Long>, Save<Compte, Compte> {
    Compte findByNumeroDuCompte(String numeroDuCompte);
    Compte findByRib(String rib);

    List<Compte> saveAll(List<Compte> comptes);
}
