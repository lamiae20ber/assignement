package ma.octo.assignement.dto;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class VirementDto {

  @NotNull(message = "Numero du compte emetteur ne doit pas être null")
  @NotBlank(message = "Numero du compte emetteur ne doit pas être vide")
  private String numeroCompteEmetteur;

  @NotNull(message = "Numero du compte benificiaire ne doit pas être null")
  @NotBlank(message = "Numero du compte benificiaire ne doit pas être vide")
  private String numeroCompteBeneficiaire;

  @NotNull(message = "Motif du virement ne doit pas être null")
  @NotBlank(message = "Motif du virement ne doit pas être vide")
  private String motifVirement;


  @Min(value = 10, message = "Le montant doit être supérieur à 10 ")
  @Max(value = 10000, message = "Le montant doit être inférieur à 10000")
  private BigDecimal montantVirement;

  private Date dateExecution;

}
