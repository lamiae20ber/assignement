package ma.octo.assignement.service;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.utils.facades.FindAll;
import ma.octo.assignement.utils.facades.Save;

import java.util.List;

public interface VirementService extends Save<Virement, Virement>, FindAll<List<Virement>> {

}
