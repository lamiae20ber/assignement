package ma.octo.assignement.service.Impl;

import lombok.AllArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.CompteService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CompteServiceImpl implements CompteService {
    private final CompteRepository compteRepository;

    @Override
    public List<Compte> findAll() {
        return compteRepository.findAll();
    }

    @Override
    public Compte findById(Long id) {
        return compteRepository.findById(id).orElse(null);
    }

    @Override
    public Compte findByNumeroDuCompte(String numeroDuCompte) {
        return compteRepository.findByNumeroDuCompte(numeroDuCompte);
    }

    @Override
    public Compte findByRib(String rib) {
        return compteRepository.findByRib(rib);
    }

    @Override
    public List<Compte> saveAll(List<Compte> comptes) {
        return compteRepository.saveAll(comptes);
    }

    @Override
    public Compte save(Compte compte) {
        return compteRepository.save(compte);
    }
}
