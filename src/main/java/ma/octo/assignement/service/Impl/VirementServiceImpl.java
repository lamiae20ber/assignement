package ma.octo.assignement.service.Impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.AuditVirement;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.AuditVirementService;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.VirementService;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class VirementServiceImpl implements VirementService {
    private final VirementRepository virementRepository;
    private final CompteService compteService;
    private final AuditVirementService auditVirementService;

    @Override
    public List<Virement> findAll() {
        return virementRepository.findAll();
    }

    @Override
    public Virement save(Virement virement) throws Exception {
        if (virement.getCompteEmetteur().getSolde().compareTo(virement.getMontantVirement()) < 0)
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant");

        Compte compteBeneficiaire = virement.getCompteBeneficiaire();
        Compte compteEmetteur = virement.getCompteEmetteur();

        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(virement.getMontantVirement()));
        compteBeneficiaire.setSolde(compteBeneficiaire.getSolde().add(virement.getMontantVirement()));

        compteService.saveAll(Arrays.asList(compteEmetteur, compteBeneficiaire));

        auditVirementService.save(new AuditVirement("Virement depuis " + compteEmetteur.getNumeroDuCompte() + " vers " + compteBeneficiaire.getNumeroDuCompte() + " d'un montant de " + virement.getMontantVirement()));


        return virementRepository.save(virement);
    }


}
