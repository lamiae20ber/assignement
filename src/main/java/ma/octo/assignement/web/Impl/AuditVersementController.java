package ma.octo.assignement.web.Impl;

import lombok.AllArgsConstructor;
import ma.octo.assignement.domain.AuditVersement;
import ma.octo.assignement.service.AuditVersementService;
import ma.octo.assignement.utils.facades.FindAll;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/auditVersements")
public class AuditVersementController implements FindAll<List<AuditVersement>> {
    private final AuditVersementService auditVersementService;

    @GetMapping("/")
    @Override
    public List<AuditVersement> findAll() {
        return auditVersementService.findAll();
    }
}
