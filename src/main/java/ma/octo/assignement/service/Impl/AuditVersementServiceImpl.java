package ma.octo.assignement.service.Impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.AuditVersement;
import ma.octo.assignement.domain.util.TypeEvenement;
import ma.octo.assignement.repository.AuditVersementRepository;
import ma.octo.assignement.service.AuditVersementService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Slf4j
@AllArgsConstructor
@Service
public class AuditVersementServiceImpl implements AuditVersementService {
    private final AuditVersementRepository auditVersementRepository;

    @Override
    public AuditVersement save(AuditVersement auditVersement) {
        log.info("Audit de l'événement {}", TypeEvenement.VERSEMENT);
        log.info("Sauvegarde Audit versement  {}", auditVersement);
        return auditVersementRepository.save(auditVersement);
    }

    @Override
    public List<AuditVersement> findAll() {
        return auditVersementRepository.findAll();
    }
}
