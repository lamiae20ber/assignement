package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CompteRepository extends JpaRepository<Compte, Long> {
    Compte findByNumeroDuCompte(String numeroDuCompte);
    Compte findByRib(String rib);

}
