package ma.octo.assignement.service.Impl;

import ma.octo.assignement.domain.AuditVersement;
import ma.octo.assignement.repository.AuditVersementRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AuditVersementServiceImplTest {

    @Mock
    private AuditVersementRepository auditVersementRepository;
    @InjectMocks
    private AuditVersementServiceImpl auditVersementService;

    @Test
    void saveOnSuccess() {
        //GIVEN
        AuditVersement auditVersement = new AuditVersement("Versement depuis BOUJAAFAR Lamiae vers le compte :010000A000001000 d'un montant de 15");

        //WHEN
        when(auditVersementRepository.save(any(AuditVersement.class))).then(returnsFirstArg());
        AuditVersement savedAuditVersement = auditVersementService.save(auditVersement);

        //THEN
        assertThat(savedAuditVersement.getMessage()).isSameAs(auditVersement.getMessage());
        assertThat(savedAuditVersement.getTypeEvenement()).isSameAs(auditVersement.getTypeEvenement());
    }

    @Test
    void findAll() {
    }
}