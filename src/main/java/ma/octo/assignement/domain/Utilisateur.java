package ma.octo.assignement.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "UTILISATEUR")
@AllArgsConstructor
@NoArgsConstructor
public class Utilisateur implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 10, nullable = false, unique = true)
    private String nomUtilisateur;

    @Column(length = 10, nullable = false)
    private String genre;

    @Column(length = 60, nullable = false)
    private String nom;

    @Column(length = 60, nullable = false)
    private String prenom;

    @Temporal(TemporalType.DATE)
    private Date dateDeNaissance;

}
