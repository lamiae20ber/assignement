package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.Impl.CompteServiceImpl;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.math.BigDecimal;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class VersementMapperTest {
    @Mock
    private CompteRepository compteRepository;
    @InjectMocks
    private CompteServiceImpl compteService ;
    private VersementMapper mapper = new VersementMapper(new ModelMapper(), compteService);

    @Test
    void convertToDtoOnSuccess() {
        Utilisateur utilisateur = new Utilisateur(1L, "user1", "Male", "last1", "first1", null);
        Compte compte = new Compte(1L, "010000A000001000", "RIB1", new BigDecimal(5), utilisateur);
        Versement versement = new Versement(3L, new BigDecimal(200), new Date(), "BOUJAAFAR Lamiae", compte, "Assignment 2021");

        VersementDto versementDTO = mapper.convertToDTO(versement);
        System.out.println(versementDTO);
        System.out.println(versementDTO.getRib());
        System.out.println(versement.getCompteBeneficiaire().getRib());
        assertThat(versementDTO.getRib()).isSameAs(versement.getCompteBeneficiaire().getRib());

    }
    @Test
    @Disabled
    void convertToDmOnAccountNotFound() throws CompteNonExistantException {

        VersementDto versementDto = new VersementDto("Lamiae","RIB1","versement",new BigDecimal(200),new Date());

        Versement versementDTO = mapper.convertToDM(versementDto);
        System.out.println(versementDTO);

    }


//    @Test
//    void convertToDMOnSuccess() {
//    }
}