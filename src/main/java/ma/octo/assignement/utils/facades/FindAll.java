package ma.octo.assignement.utils.facades;

import java.util.List;

public interface FindAll<E> {
    E findAll();
}
