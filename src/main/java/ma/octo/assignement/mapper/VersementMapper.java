package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.service.CompteService;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.springframework.stereotype.Component;

@Component
public class VersementMapper implements Mapper<Versement, VersementDto> {

    private final ModelMapper modelMapper;
    private final CompteService compteService;

    public VersementMapper(ModelMapper modelMapper, CompteService compteService) {
        this.compteService = compteService;
        modelMapper.getConfiguration()
                .setFieldMatchingEnabled(true)
                .setFieldAccessLevel(Configuration.AccessLevel.PRIVATE);
        this.modelMapper = modelMapper;
    }

    @Override
    public Versement convertToDM(VersementDto versementDto) throws CompteNonExistantException {
        Compte compteBenificiare = compteService.findByRib(versementDto.getRib());
        if (compteBenificiare == null)
            throw new CompteNonExistantException("Compte Benificiare Non existant");
        Versement versement = modelMapper.map(versementDto, Versement.class);
        versement.setCompteBeneficiaire(compteBenificiare);
        return versement;
    }

    @Override
    public VersementDto convertToDTO(Versement versement) {
        VersementDto versementDto = modelMapper.map(versement, VersementDto.class);
        versementDto.setRib(versement.getCompteBeneficiaire().getRib());
        return versementDto;
    }
}
