package ma.octo.assignement.service.Impl;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import ma.octo.assignement.domain.AuditVersement;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.AuditVersementService;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.VersementService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class VersementServiceImpl implements VersementService {
    private final VersementRepository versementRepository;
    private final CompteService compteService;
    private final AuditVersementService auditVersementService;


    @Override
    public List<Versement> findAll() {
        return versementRepository.findAll();
    }

    @Override
    public Versement save(Versement versement) throws Exception {
        Compte compteBeneficiaire = versement.getCompteBeneficiaire();

        compteBeneficiaire.setSolde(compteBeneficiaire.getSolde().add(versement.getMontant()));

        compteService.save(compteBeneficiaire);

        auditVersementService.save(new AuditVersement("Versement depuis " + versement.getNomPrenomEmetteur() + " vers le compte :" + compteBeneficiaire.getNumeroDuCompte() + " d'un montant de " + versement.getMontant()));

        return versementRepository.save(versement);
    }
}
