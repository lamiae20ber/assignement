package ma.octo.assignement.service;

import ma.octo.assignement.domain.AuditVersement;
import ma.octo.assignement.utils.facades.FindAll;
import ma.octo.assignement.utils.facades.Save;

import java.util.List;

public interface AuditVersementService extends Save<AuditVersement, AuditVersement>, FindAll<List<AuditVersement>> {

}
