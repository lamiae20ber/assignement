package ma.octo.assignement.utils.facades;

public interface Delete<E> {
    E delete(E e);
}
