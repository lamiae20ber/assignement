package ma.octo.assignement.web.Impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.service.VirementService;
import ma.octo.assignement.utils.facades.FindAll;
import ma.octo.assignement.utils.facades.Save;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/virements")
public class VirementController implements Save<ResponseEntity, VirementDto>, FindAll<List<VirementDto>> {
    private final VirementService virementService;
    private final VirementMapper virementMapper;

    @GetMapping("/")
    @Override
    public List<VirementDto> findAll() {
        return virementMapper.convertToDTOs(virementService.findAll());
    }

    @PostMapping("/")
    @Override
    public ResponseEntity<?> save(@Valid @RequestBody VirementDto virementDto) throws Exception {
        if (virementDto == null)
            return ResponseEntity.badRequest().body("Le request body est corrompu ou incomplet.");
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(virementMapper.convertToDTO(virementService.save(virementMapper.convertToDM(virementDto))));
    }

}
