package ma.octo.assignement.utils.facades;

public interface FindById <E,T>{
    E findById(T t);
}
