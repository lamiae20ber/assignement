package ma.octo.assignement.service.Impl;

import ma.octo.assignement.domain.AuditVersement;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.AuditVersementRepository;
import ma.octo.assignement.repository.CompteRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CompteServiceImplTest {

    @Mock
    private CompteRepository compteRepository;
    @InjectMocks
    private CompteServiceImpl compteService;

    @Test
    void saveOnSuccess() {
        Utilisateur utilisateur = new Utilisateur(1L,"user1","Male","last1","first1",null);
        //GIVEN
        Compte compte = new Compte(1L,"010000A000001000","RIB1", new BigDecimal(5), utilisateur);
        //WHEN
        when(compteRepository.save(any(Compte.class))).then(returnsFirstArg());
        Compte savedCompte = compteService.save(compte);

        //THEN
        assertThat(savedCompte.getNumeroDuCompte()).isSameAs(compte.getNumeroDuCompte());
        assertThat(savedCompte.getRib()).isSameAs(compte.getRib());
        assertThat(savedCompte.getUtilisateur()).isSameAs(compte.getUtilisateur());
    }


}