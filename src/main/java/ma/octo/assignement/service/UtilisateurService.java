package ma.octo.assignement.service;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.utils.facades.FindAll;
import ma.octo.assignement.utils.facades.FindById;

import java.util.List;

public interface UtilisateurService extends FindAll<List<Utilisateur>>, FindById<Utilisateur, Long> {
}
