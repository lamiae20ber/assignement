package ma.octo.assignement.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ma.octo.assignement.domain.util.TypeEvenement;
import javax.persistence.*;

@MappedSuperclass
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Audit {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    @Column(length = 100)
    protected String message;

    @Enumerated(EnumType.STRING)
    protected TypeEvenement typeEvenement;

    public Audit(String message, TypeEvenement typeEvenement) {
        this.message = message;
        this.typeEvenement = typeEvenement;
    }
}
