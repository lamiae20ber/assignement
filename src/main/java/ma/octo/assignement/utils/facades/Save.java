package ma.octo.assignement.utils.facades;

import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;

public interface Save<E,T> {
    E save(T t) throws Exception;
}
