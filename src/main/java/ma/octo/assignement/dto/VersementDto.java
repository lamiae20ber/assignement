package ma.octo.assignement.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VersementDto {
    @NotNull(message = "Nom et prenom du compte emetteur ne doit pas être null")
    @NotBlank(message = "Nom et prenom du compte emetteur ne doit pas être vide")
    private String nomPrenomEmetteur;

    @NotNull(message = "Numero du compte binificiaire ne doit pas être null")
    @NotBlank(message = "Numero du compte binificiaire ne doit pas être vide")
    private String rib;

    @NotNull(message = "Motif du virement ne doit pas être null")
    @NotBlank(message = "Motif du virement ne doit pas être vide")
    private String motif;

    @Min(value = 10, message = "Le montant doit être supérieur à 10 ")
    @Max(value = 10000, message = "Le montant doit être inférieur à 10000")
    private BigDecimal montant;
    private Date dateExecution;
}
