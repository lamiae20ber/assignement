package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.service.CompteService;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.springframework.stereotype.Component;

@Component
public class VirementMapper implements Mapper<Virement, VirementDto> {

    private final ModelMapper modelMapper;
    private final CompteService compteService;

    public VirementMapper(ModelMapper modelMapper, CompteService compteService) {
        this.compteService = compteService;
        modelMapper.getConfiguration()
                .setFieldMatchingEnabled(true)
                .setFieldAccessLevel(Configuration.AccessLevel.PRIVATE);
        this.modelMapper = modelMapper;
    }

    @Override
    public Virement convertToDM(VirementDto virementDto) throws CompteNonExistantException {
        Compte compteEmetteur = compteService.findByNumeroDuCompte(virementDto.getNumeroCompteEmetteur());
        if (compteEmetteur == null)
            throw new CompteNonExistantException("Compte Emetteur Non existant");
        Compte compteBenificiare = compteService.findByNumeroDuCompte(virementDto.getNumeroCompteBeneficiaire());
        if (compteBenificiare == null)
            throw new CompteNonExistantException("Compte Benificiare Non existant");
        Virement virement = modelMapper.map(virementDto, Virement.class);
        virement.setCompteEmetteur(compteEmetteur);
        virement.setCompteBeneficiaire(compteBenificiare);
        return virement;
    }

    @Override
    public VirementDto convertToDTO(Virement virement) {
        VirementDto virementDto = modelMapper.map(virement, VirementDto.class);
        virementDto.setNumeroCompteBeneficiaire(virement.getCompteBeneficiaire().getNumeroDuCompte());
        virementDto.setNumeroCompteEmetteur(virement.getCompteEmetteur().getNumeroDuCompte());
        return virementDto;
    }
}
