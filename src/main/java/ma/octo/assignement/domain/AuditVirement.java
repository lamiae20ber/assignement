package ma.octo.assignement.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import ma.octo.assignement.domain.util.TypeEvenement;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "AUDIT_VIREMENT")
@NoArgsConstructor
public class AuditVirement extends Audit {
    public AuditVirement(String message) {
        super(message, TypeEvenement.VIREMENT);
    }
}
