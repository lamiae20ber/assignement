package ma.octo.assignement.web.Impl;

import lombok.AllArgsConstructor;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.service.UtilisateurService;
import ma.octo.assignement.utils.facades.FindAll;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/utilisateurs")
public class UtilisateurController  implements FindAll<List<Utilisateur>> {
    private final UtilisateurService utilisateurService;

    @GetMapping("/")
    @Override
    public List<Utilisateur> findAll() {
        return utilisateurService.findAll();
    }
}
