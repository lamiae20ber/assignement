package ma.octo.assignement.web.Impl;

import lombok.AllArgsConstructor;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.service.VersementService;
import ma.octo.assignement.utils.facades.FindAll;
import ma.octo.assignement.utils.facades.Save;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/versements")
public class VersementController implements FindAll<List<Versement>>, Save<ResponseEntity, VersementDto> {
    private final VersementService versementService;
    private final VersementMapper versementMapper;

    @GetMapping("/")
    @Override
    public List<Versement> findAll() {
        return versementService.findAll();
    }

    @PostMapping("/")
    @Override
    public ResponseEntity save(@Valid @RequestBody VersementDto versementDto) throws Exception {
        if (versementDto == null)
            return ResponseEntity.badRequest().body("Le request body est corrompu ou incomplet.");
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(versementMapper.convertToDTO(versementService.save(versementMapper.convertToDM(versementDto))));
    }
}
