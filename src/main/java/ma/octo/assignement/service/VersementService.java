package ma.octo.assignement.service;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.utils.facades.FindAll;
import ma.octo.assignement.utils.facades.Save;

import java.util.List;

public interface VersementService extends Save<Versement, Versement>, FindAll<List<Versement>> {
}
